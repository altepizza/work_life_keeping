import os
from bson import ObjectId
from datetime import datetime
from enum import Enum
from fastapi import FastAPI, Security, Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security.api_key import APIKeyQuery, APIKeyCookie, APIKeyHeader, APIKey
from pydantic import BaseModel, Field
from pymongo import MongoClient
from starlette.status import HTTP_403_FORBIDDEN
from typing import Optional

API_KEY = os.environ['API_KEY']
API_KEY_NAME = 'access_token'
MONGODB_HOST = os.environ['MONGO_DB_HOST']

api_key_query = APIKeyQuery(name=API_KEY_NAME, auto_error=False)
api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)


async def get_api_key(
        api_key_query: str = Security(api_key_query),
        api_key_header: str = Security(api_key_header),
):
    if api_key_query == API_KEY:
        return api_key_query
    elif api_key_header == API_KEY:
        return api_key_header
    else:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN,
                            detail="Could not validate credentials")


client = MongoClient(f'mongodb://root:example@{MONGODB_HOST}', 27017)
db = client.test
app = FastAPI()

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError('Invalid objectid')
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')


class TypesEnum(str, Enum):
    check_in = 'check_in'
    check_out = 'check_out'


class Timing(BaseModel):
    id: Optional[PyObjectId] = Field(alias='_id')
    date_time: datetime
    type: TypesEnum

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}


# @app.delete("/timings/{timing_id}")
# async def delete_timing(timing_id):
#     query = {"_id": timing_id}
#     db.timings.delete_one(query)
#     return


@app.get("/timings")
async def list_timings(api_key: APIKey = Depends(get_api_key)):
    timings = []
    for timing in db.timings.find():
        timings.append(Timing(**timing))
    return {'timings': timings}


@app.post("/timings")
async def create_timing(timing: Timing,
                        api_key: APIKey = Depends(get_api_key)):
    if hasattr(timing, 'id'):
        delattr(timing, 'id')
    ret = db.timings.insert_one(timing.dict(by_alias=True))
    timing.id = ret.inserted_id
    return {'timing': timing}
